from tkinter import*
import re
class MyFirstGUI:
    i = 0
    MyTokens = []
    InToken = []
    def __init__(self, master):
        userInput = ""
        self.master = master
        master.title("Lexical Analyzer for TinyPie")
        self.inputLabel = Label(master,text = "Source Code Input").grid(row = 0, column = 0,sticky = E)
        self.outputLabel = Label(master,text="Lexical Analyzed Result").grid(row = 0, column = 2, sticky = E)
        self.outputParser = Label(master, text= "Parser tree").grid(row = 0, column = 4, sticky = E)
        self.input = Text(master, height = 25, width = 40)
        self.output = Text(master, height = 25, width = 40)
        self.parserOutput = Text(master, heigh = 25, width = 45)
        self.input.grid(row=1, column=0, columnspan = 2, sticky="nsew", padx=2, pady=2)
        self.output.grid(row=1, column=2, columnspan = 2, sticky="nsew", padx=2, pady=2)
        self.parserOutput.grid(row=1, column = 4, columnspan = 2, sticky = "nsew", padx= 2, pady=2)
        self.lineLabel = Label(master, text = "Current processing line").grid(row = 2, column = 0, sticky = E)
        self.line= StringVar()
        self.line.set(self.i)
        self.output.insert(END,"Yo")
        self.button = Button(master, text ="Next Line", command = self.submitline).grid(row = 3, column = 0, sticky = E)

        self.output.delete("1.0", END)
        self.processingline = Label(master, textvariable = self.line).grid(row = 2, column = 1, stick = E)
        self.quitbutton = Button(master, text = "Quit", command = master.destroy).grid(row = 3, column = 5, sticky = E)
    def submitline(self):
        print("I am here")
        userInput = self.input.get("1.0", END)
        lines = userInput.splitlines()
        if(self.i < len(lines)-1):
            self.lexer(lines[self.i])
            self.parserOutput.insert(END,"Parse tree for line " + str(self.i+1)+"\n\n")
            self.parser()
            self.i +=1
            self.line.set(self.i)
            self.MyTokens = []
        else:
            self.i = 0
            self.output.delete("1.0", END)
            self.parserOutput.delete("1.0", END)
    def lexer(self,line):
        line = line.lstrip()
        while line:
            if(re.match(r'if|else|int|print|float',line)):
                result = re.match(r'if|else|int|print|float',line)
                endpos = result.end()
                result = result.group(0)
                line = line[endpos:].lstrip()
                self.output.insert(END,"<Keyword " + str(result) + ">\n")
                self.MyTokens.append("Keyword")
                self.MyTokens.append(result)

            elif(re.match(r'[A-z]+[0-9]*',line)):
                result = re.match(r'(?!\d)\w*\d*',line)
                endpos = result.end()
                result = result.group(0)
                line = line[endpos:].lstrip()
                self.output.insert(END,"<Identifier " + str(result)+ ">\n")
                self.MyTokens.append("Identifier")
                self.MyTokens.append(result)

            elif(re.match(r'[=+>*]',line)):
                result = re.match(r'[=+*>]',line)
                endpos = result.end()
                result = result.group(0)
                line = line[endpos:].lstrip()
                self.output.insert(END,"<Operator " + str(result) + ">\n")
                self.MyTokens.append("Operator")
                self.MyTokens.append(result)

            elif(re.match(r'[():;"]',line)):
                result = re.match(r'[():";]',line)
                endpos = result.end()
                result = result.group(0)
                if(result == '"'):
                    line = line[1:].lstrip()
                    self.output.insert(END,"<Separators " + str(result)+ ">\n")
                    self.MyTokens.append("Separator")
                    self.MyTokens.append(result)
                    result = ""
                    while (line[0] != '"'):
                        result += line[0]
                        line = line[1:]
                    self.output.insert(END, "<String_Literal " + str(result) + ">\n")
                    self.MyTokens.append("String_Literal")
                    self.MyTokens.append(result)
                    result = line[0]
                    line = line[1:].lstrip()
                    self.output.insert(END,"<Separators " + str(result)+ ">\n")
                    self.MyTokens.append("Separator")
                    self.MyTokens.append(result)
                else:
                    line = line[endpos:].lstrip()
                    self.output.insert(END,"<Separators " + str(result)+ ">\n")
                    self.MyTokens.append("Separator")
                    self.MyTokens.append(result)
            elif(re.match(r'\d+[.]\d+', line)): #(?:"\w*")|\d*
                line = line.lstrip()
                result = re.match(r'\d+[.]\d+',line)
                endpos = result.end()
                result = result.group(0)
                line = line[endpos:].lstrip()
                self.output.insert(END,"<Float_Literal " + str(result) + ">\n")
                self.MyTokens.append("Float_Literal")
                self.MyTokens.append(result)    
            elif(re.match(r'^\d+', line)): #(?:"\w*")|\d*
                line = line.lstrip()
                result = re.match(r'^\d+',line)
                endpos = result.end()
                result = result.group(0)
                line = line[endpos:].lstrip()
                self.output.insert(END,"<Int_Literal " + str(result) + ">\n")
                self.MyTokens.append("Int_Literal")
                self.MyTokens.append(result)  
    
    def accept(self):
        if(self.MyTokens):
            self.InToken.clear()
            self.InToken.append((self.MyTokens.pop(0),self.MyTokens.pop(0)))
        else:
            self.InToken.clear()
    def math(self):
        self.parserOutput.insert(END,"\nParent node math, finding child nodes\n")
        if(self.InToken[0][0] == "Int_Literal"):
            self.parserOutput.insert(END, "child node (internal): Int_Literal \n")
            self.parserOutput.insert(END, "Int_Literal has child node:" + str(self.InToken[0][1]) +"\n")
            self.accept()
            if(self.InToken[0][1] == "*"):
                self.parserOutput.insert(END, "child node (internal): * \n")
                self.accept()
                self.parserOutput.insert(END, "child node (internal): math \n")
                self.math()
            else:
                print("expecting a *")
        elif(self.InToken[0][0] == "Float_Literal"):
            self.parserOutput.insert(END, "child node (internal): Float_Literal \n")
            self.parserOutput.insert(END, "Float_Literal has child node:" + str(self.InToken[0][1]) +"\n")
            self.accept()
            if(self.InToken[0][1] == "+"):
                self.parserOutput.insert(END, "child node (internal): + \n")
                self.accept()
                self.parserOutput.insert(END, "child node (internal): Math \n")
                self.math()
            else:
                return
        else:
            return

    def exp(self):
        self.parserOutput.insert(END,"Parent node expression, finding child nodes\n")
        dataType = self.InToken[0][1]
        if(dataType == "float" or dataType == "int"):
            self.parserOutput.insert(END, "child node (internal): keyword \n")
            self.parserOutput.insert(END, "keyword has child node:" + str(self.InToken[0][1]) +"\n")
            self.accept()
            typeT,token = self.InToken.pop(0)
            if(typeT == "Identifier"):
                self.parserOutput.insert(END, "child node (internal): Identifier \n")
                self.parserOutput.insert(END, "Identifier has child node:" + token +"\n")
                self.accept()
            else:
                print("expecting an identifier first")
        elif(dataType == "if"):
            self.if_exp()
            return
        elif(dataType == "print"):
            self.print_exp()
            return
        else:
            print("Expecting a data type first")
        if(self.InToken[0][1] == '='):
            self.parserOutput.insert(END,"child node (token): = \n")
            self.accept()
            self.parserOutput.insert(END, "child node (internal): Math \n")
            self.math()
    def if_exp(self):
        self.parserOutput.insert(END, "child node (internal): Keyword \n")
        self.parserOutput.insert(END, "Keyword has child node:" + str(self.InToken[0][1]) +"\n")
        self.accept()
        self.comparison_exp()
        return
    def comparison_exp(self):
        if(self.InToken):
            if(self.InToken[0][0] == "Separator"):
                self.parserOutput.insert(END, "child node (internal): Separator \n")
                self.parserOutput.insert(END,"Separator has child node (token):" + str(self.InToken[0][1]) + "\n")
                self.accept()
                self.comparison_exp()
            elif(self.InToken[0][0] == "Identifier"):
                self.parserOutput.insert(END, "child node (internal): Identifier \n")
                self.parserOutput.insert(END, "Identifier has child node:" + str(self.InToken[0][1]) +"\n")
                self.accept()
                self.comparison_exp()
            elif(self.InToken[0][0] == "Operator"):
                self.parserOutput.insert(END, "child node (internal): Operator \n")
                self.parserOutput.insert(END, "Identifier has child node:" + str(self.InToken[0][1]) +"\n")
                self.accept()
                self.comparison_exp()
    def print_exp(self):
        self.parserOutput.insert(END, "child node (internal): Keyword \n")
        self.parserOutput.insert(END, "Keyword has child node:" + str(self.InToken[0][1]) +"\n")
        self.accept()
        self.print()
    
    def print(self):
        if(self.InToken):
            if(self.InToken[0][0] == "Separator"):
                self.parserOutput.insert(END, "child node (internal): Separator \n")
                self.parserOutput.insert(END,"Separator has child node (token):" + str(self.InToken[0][1]) + "\n")
                self.accept()
                self.print()
            elif(self.InToken[0][0] == "String_Literal"):
                self.parserOutput.insert(END, "child node (internal): String_Literal \n")
                self.parserOutput.insert(END, "String_Literal has child node:" + str(self.InToken[0][1]) +"\n")
                self.accept()
                self.print()


    def parser(self):
        self.accept()
        self.exp()
        if(self.InToken):
            if(self.InToken[0][1] == ";"):
                self.parserOutput.insert(END,"Parse tree building success!\n\n")
                return
        else:
            return
    def destroy():
        self.destroy()
    

root = Tk()
my_gui = MyFirstGUI(root)
root.mainloop()
